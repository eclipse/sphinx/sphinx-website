<?php

/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *		Eclipse Foundation - Initial version
 *		Stephan Eberle - Changes for Sphinx
 *		Idrissa Dieng - Changes for Sphinx
 *      Yue Ma - Changes for Sphinx
 * 		Balázs Grill - Updated product name
 *******************************************************************************/

	# Set the theme for your project's web pages.
	# See the Committer Tools "How Do I" for list of themes
	# https://dev.eclipse.org/committers/
	# Optional: defaults to system theme
	$theme = "solstice";

	$Nav->setLinkList(array());

	# Define your project-wide Nav bars here.
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# these are optional

	$Nav->addNavSeparator("Project Home", "/sphinx/");
		$Nav->addCustomNav("Download", "/sphinx/download.php", "_self", 2);
		$Nav->addCustomNav("Documentation", "/sphinx/documentation.php", "_self", 2);
		$Nav->addCustomNav("Support", "/sphinx/support.php", "_self", 2);
		$Nav->addCustomNav("Getting Involved", "/sphinx/gettinginvolved.php", "_self", 2);
		$Nav->addCustomNav("Wiki", "http://wiki.eclipse.org/Sphinx", "_self", 2);

	# Define keywords, author and title here, or in each PHP page specifically
	$pageKeywords	= "eclipse, modeling, domain specific language, dsl, ide";
	$pageAuthor		= "Stephan Eberle";
	$pageTitle 		= "Eclipse Sphinx&trade;";

	# Define additional CSS or other pre-body headers
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="style.css"/>');

	# Top navigation bar
	# To override and replace the navigation with your own, uncomment the line below.
	# $Menu->setMenuItemList(array());
	# $Menu->addMenuItem("Home", "/sphinx", "_self");
	# $Menu->addMenuItem("Download", "/sphinx/download.php", "_self");
	# $Menu->addMenuItem("Documentation", "/sphinx/documentation.php", "_self");
	# $Menu->addMenuItem("Support", "/sphinx/support.php", "_self");
	# $Menu->addMenuItem("Getting Involved", "/sphinx/gettinginvolved.php", "_self");

	# To enable occasional Eclipse Foundation Promotion banners on your pages (EclipseCon, etc)
	# $App->Promotion = TRUE;

	# If you have Google Analytics code, use it here
	# $App->SetGoogleAnalyticsTrackingCode("YOUR_CODE");
?>